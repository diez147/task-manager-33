package ru.tsc.babeshko.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

public interface IEndpoint {

    @NotNull
    String NAMESPACE = "http://endpoint.tm.babeshko.tsc.ru/";

}