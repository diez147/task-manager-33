package ru.tsc.babeshko.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.dto.request.TaskBindToProjectRequest;
import ru.tsc.babeshko.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.tsc.babeshko.tm.dto.response.TaskBindToProjectResponse;
import ru.tsc.babeshko.tm.dto.response.TaskUnbindFromProjectResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IProjectTaskEndpoint extends IEndpoint {

    @NotNull
    String NAME = "ProjectTaskEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectTaskEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + NAME + "?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(NAMESPACE, PART);
        return Service.create(url, qName).getPort(IProjectTaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskBindToProjectRequest request
    );

    @NotNull
    @WebMethod
    TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskUnbindFromProjectRequest request
    );

}