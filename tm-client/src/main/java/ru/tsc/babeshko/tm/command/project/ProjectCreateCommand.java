package ru.tsc.babeshko.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.dto.request.ProjectCreateRequest;
import ru.tsc.babeshko.tm.util.TerminalUtil;

import java.util.Date;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-create";

    @NotNull
    public static final String DESCRIPTION = "Create new project.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        @NotNull final String description = TerminalUtil.nextLine();
        System.out.println("[ENTER DATE BEGIN:]");
        @Nullable final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("[ENTER DATE END:]");
        @Nullable final Date dateEnd = TerminalUtil.nextDate();
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(name, description, dateBegin, dateEnd);
        getProjectEndpointClient().createProject(request);
    }

}