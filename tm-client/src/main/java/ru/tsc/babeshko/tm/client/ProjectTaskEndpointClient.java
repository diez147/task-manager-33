package ru.tsc.babeshko.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.api.client.IProjectTaskEndpointClient;
import ru.tsc.babeshko.tm.dto.request.TaskBindToProjectRequest;
import ru.tsc.babeshko.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.tsc.babeshko.tm.dto.response.TaskBindToProjectResponse;
import ru.tsc.babeshko.tm.dto.response.TaskUnbindFromProjectResponse;

public class ProjectTaskEndpointClient extends AbstractEndpointClient implements IProjectTaskEndpointClient {

    @NotNull
    @Override
    @SneakyThrows
    public TaskBindToProjectResponse bindTaskToProject(@NotNull final TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull final TaskUnbindFromProjectRequest request) {
        return call(request, TaskUnbindFromProjectResponse.class);
    }

}