package ru.tsc.babeshko.tm.api.client;

import ru.tsc.babeshko.tm.api.endpoint.IAuthEndpoint;

public interface IAuthEndpointClient extends IAuthEndpoint, IEndpointClient {
}