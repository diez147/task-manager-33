package ru.tsc.babeshko.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.dto.request.TaskListByProjectIdRequest;
import ru.tsc.babeshko.tm.dto.response.TaskListByProjectIdResponse;
import ru.tsc.babeshko.tm.model.Task;
import ru.tsc.babeshko.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-project-id";

    @NotNull
    public static final String DESCRIPTION = "Show task list by project id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID:]");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(projectId);
        @NotNull final TaskListByProjectIdResponse response = getTaskEndpointClient().listTaskByProjectId(request);
        @Nullable final List<Task> tasks = response.getTasks();
        renderTasks(tasks);
    }

}