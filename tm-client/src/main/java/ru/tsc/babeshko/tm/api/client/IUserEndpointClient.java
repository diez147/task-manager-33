package ru.tsc.babeshko.tm.api.client;

import ru.tsc.babeshko.tm.api.endpoint.IUserEndpoint;

public interface IUserEndpointClient extends IUserEndpoint, IEndpointClient {
}