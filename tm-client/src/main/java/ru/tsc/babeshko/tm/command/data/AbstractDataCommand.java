package ru.tsc.babeshko.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.api.client.IDomainEndpointClient;
import ru.tsc.babeshko.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public IDomainEndpointClient getDomainEndpointClient() {
        return serviceLocator.getDomainEndpointClient();
    }

}