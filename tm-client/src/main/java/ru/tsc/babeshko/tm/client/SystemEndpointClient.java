package ru.tsc.babeshko.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.api.client.ISystemEndpointClient;
import ru.tsc.babeshko.tm.dto.request.ServerAboutRequest;
import ru.tsc.babeshko.tm.dto.request.ServerVersionRequest;
import ru.tsc.babeshko.tm.dto.response.ServerAboutResponse;
import ru.tsc.babeshko.tm.dto.response.ServerVersionResponse;

public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpointClient {

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return call(request, ServerAboutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return call(request, ServerVersionResponse.class);
    }

}