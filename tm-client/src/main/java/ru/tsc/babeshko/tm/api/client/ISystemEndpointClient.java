package ru.tsc.babeshko.tm.api.client;

import ru.tsc.babeshko.tm.api.endpoint.ISystemEndpoint;

public interface ISystemEndpointClient extends ISystemEndpoint, IEndpointClient {
}