package ru.tsc.babeshko.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.api.client.IEndpointClient;
import ru.tsc.babeshko.tm.command.AbstractCommand;
import ru.tsc.babeshko.tm.enumerated.Role;

import java.net.Socket;

public class ConnectCommand extends AbstractCommand {

    @NotNull
    public static final String NAME = "connect";

    @NotNull
    public static final String DESCRIPTION = "Connect to server.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }


    @Override
    @SneakyThrows
    public void execute() {
        try {
            @NotNull final IEndpointClient endpointClient = serviceLocator.getConnectionEndpointClient();
            @Nullable final Socket socket = endpointClient.connect();
            serviceLocator.getAuthEndpointClient().setSocket(socket);
            serviceLocator.getSystemEndpointClient().setSocket(socket);
            serviceLocator.getDomainEndpointClient().setSocket(socket);
            serviceLocator.getProjectEndpointClient().setSocket(socket);
            serviceLocator.getProjectTaskEndpointClient().setSocket(socket);
            serviceLocator.getTaskEndpointClient().setSocket(socket);
            serviceLocator.getUserEndpointClient().setSocket(socket);
        } catch (@NotNull final Exception e) {
            serviceLocator.getLoggerService().error(e);
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}