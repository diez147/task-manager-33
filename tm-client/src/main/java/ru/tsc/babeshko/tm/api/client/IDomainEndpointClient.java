package ru.tsc.babeshko.tm.api.client;

import ru.tsc.babeshko.tm.api.endpoint.IDomainEndpoint;

public interface IDomainEndpointClient extends IDomainEndpoint, IEndpointClient {
}