package ru.tsc.babeshko.tm.api.client;

import ru.tsc.babeshko.tm.api.endpoint.IProjectEndpoint;

public interface IProjectEndpointClient extends IProjectEndpoint, IEndpointClient {
}
