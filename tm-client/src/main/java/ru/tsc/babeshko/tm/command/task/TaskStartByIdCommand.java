package ru.tsc.babeshko.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-start-by-id";

    @NotNull
    public static final String DESCRIPTION = "Start task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(id, Status.IN_PROGRESS);
        getTaskEndpointClient().changeTaskStatusById(request);
    }

}