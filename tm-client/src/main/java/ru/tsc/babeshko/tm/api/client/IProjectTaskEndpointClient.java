package ru.tsc.babeshko.tm.api.client;

import ru.tsc.babeshko.tm.api.endpoint.IProjectTaskEndpoint;

public interface IProjectTaskEndpointClient extends IProjectTaskEndpoint, IEndpointClient {
}