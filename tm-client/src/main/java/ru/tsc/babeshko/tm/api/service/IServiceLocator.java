package ru.tsc.babeshko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.api.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ITaskEndpointClient getTaskEndpointClient();

    @NotNull
    IProjectEndpointClient getProjectEndpointClient();

    @NotNull
    IProjectTaskEndpointClient getProjectTaskEndpointClient();

    @NotNull
    IUserEndpointClient getUserEndpointClient();

    @NotNull
    IAuthEndpointClient getAuthEndpointClient();

    @NotNull
    ISystemEndpointClient getSystemEndpointClient();

    @NotNull
    IDomainEndpointClient getDomainEndpointClient();

    @NotNull
    IEndpointClient getConnectionEndpointClient();

}