package ru.tsc.babeshko.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.dto.request.ProjectListRequest;
import ru.tsc.babeshko.tm.dto.response.ProjectListResponse;
import ru.tsc.babeshko.tm.enumerated.Sort;
import ru.tsc.babeshko.tm.model.Project;
import ru.tsc.babeshko.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-list";

    @NotNull
    public static final String DESCRIPTION = "Show project list.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("[ENTER SORT:]");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(sort);
        @NotNull final ProjectListResponse response = getProjectEndpointClient().listProject(request);
        @Nullable final List<Project> projects = response.getProjects();
        int index = 1;
        for (Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }

}