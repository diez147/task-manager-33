package ru.tsc.babeshko.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.api.service.IServiceLocator;
import ru.tsc.babeshko.tm.api.service.IUserService;
import ru.tsc.babeshko.tm.dto.request.AbstractUserRequest;
import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.exception.system.AccessDeniedException;
import ru.tsc.babeshko.tm.model.User;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    @Getter
    @Nullable
    private IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void check(@Nullable AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
    }

    protected void check(@Nullable AbstractUserRequest request, @Nullable Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final IServiceLocator serviceLocator = getServiceLocator();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        @Nullable final Role roleUser = user.getRole();
        if (roleUser != role) throw new AccessDeniedException();
    }

}