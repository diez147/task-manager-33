package ru.tsc.babeshko.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.api.endpoint.*;
import ru.tsc.babeshko.tm.api.repository.IProjectRepository;
import ru.tsc.babeshko.tm.api.repository.ITaskRepository;
import ru.tsc.babeshko.tm.api.repository.IUserRepository;
import ru.tsc.babeshko.tm.api.service.*;
import ru.tsc.babeshko.tm.endpoint.*;
import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.model.User;
import ru.tsc.babeshko.tm.repository.ProjectRepository;
import ru.tsc.babeshko.tm.repository.TaskRepository;
import ru.tsc.babeshko.tm.repository.UserRepository;
import ru.tsc.babeshko.tm.service.*;
import ru.tsc.babeshko.tm.util.DateUtil;
import ru.tsc.babeshko.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final Backup backup = new Backup(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    {
        registry(systemEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(projectTaskEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
    }

    private void initDemoData() {
        @NotNull final User usual = userService.create("user", "user", "user@user.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);
        projectService.create(
                usual.getId(), "Project 1", "1st Description",
                DateUtil.toDate("15.11.2019"), DateUtil.toDate("15.11.2019")
        ).setStatus(Status.IN_PROGRESS);
        projectService.create(
                usual.getId(), "Project 2", "2nd Description",
                DateUtil.toDate("21.12.2009"),DateUtil.toDate("15.07.2020")
        ).setStatus(Status.COMPLETED);
        projectService.create(
                usual.getId(), "Project 3", "3rd Description",
                DateUtil.toDate("01.10.2017"),DateUtil.toDate("")
        ).setStatus(Status.IN_PROGRESS);
        taskService.create(
                usual.getId(), "Task 1", "1st Task Description",
                DateUtil.toDate("14.05.2014"),DateUtil.toDate("")
        ).setStatus(Status.IN_PROGRESS);
        projectService.create(usual.getId(), "Project 4", "4th Description").setStatus(Status.NOT_STARTED);
        taskService.create(admin.getId(), "Task 2", "2nd Task Description").setStatus(Status.NOT_STARTED);
        taskService.create(admin.getId(), "Task 3", "3nd Task Description").setStatus(Status.NOT_STARTED);
    }

    public void run() {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TO TASK-MANAGER SERVER**");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER IS SHUTTING DOWN **");
        backup.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}